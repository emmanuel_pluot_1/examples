import pytest

from squad.import_example import a, b, c

@pytest.mark.skip(reason='Comment this for demo')
def test_import_b():
    # Given
    new_test = 'Youll nevah catch me aliiiiiive'

    # When
    a.test = new_test
    b_test = b.get_test()

    # Then
    assert b_test == new_test

def test_import_c():
    # Given
    new_test = 'Youll nevah catch me aliiiiiive'

    # When
    a.test = new_test
    c_test = c.get_test()

    # Then
    assert c_test == new_test

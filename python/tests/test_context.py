import pytest

from squad.context.examples import example_func, ExampleCls

def test_example_func():
    # Given
    class Test: pass

    # When
    with example_func(Test) as truc:
        truc_val = truc
        res1 = Test().lol()

    # Then
    assert truc_val == 2
    assert res1 == 'wow'
    with pytest.raises(AttributeError):
        Test().lol()

def test_example_cls():
    # Given
    class Test: pass

    # When
    with ExampleCls(Test) as truc:
        truc_val = truc
        res1 = Test().lol()

    # Then
    assert truc_val == 2
    assert res1 == 'wow'
    with pytest.raises(AttributeError):
        Test().lol()

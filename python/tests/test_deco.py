from squad.decorators.examples import (
    call_twice,
    call_twice_mult,
    add_hello,
    noned
)

def test_call_twice():
    # Given
    @call_twice
    def my_func():
        return 2

    # When
    res = my_func()

    # Then
    assert res == (2, 2)

def test_call_twice_mult():
    # Given
    @call_twice_mult(4)
    def my_func():
        return 2

    # When
    res = my_func()

    # Then
    assert res == (8, 8)

def test_add_hello():
    # Given
    @add_hello
    class Test:
        pass

    # When
    res = Test().hello()

    # Then
    assert res == 'wow'

def test_add_hello():
    # Given
    @noned
    class Test:
        pass

    @noned
    def my_func():
        return 'I do not exist!!'

    # When

    # Then
    assert Test is None
    assert my_func is None

def test_int_inf_256_are_refs():
    # Given
    def gen_int(v): # Use this method to ensure no further optimization caching bigger integers
        return v + 1
    a = gen_int(12)
    b = gen_int(12)
    c = gen_int(300)
    d = gen_int(300)

    # When

    # Then
    assert a == b
    assert a is b
    assert c == d
    assert c is not d

def test_lambda_in_loop():
    # Given
    a = []
    for i in range(10):
        a.append(lambda: i)

    # When
    res = [l() for l in a]

    # Then
    assert res == [
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9
    ]

def test_for_else():
    # Given

    # When
    for i in range(1):
        res1 = 0
    else:
        res1 = 1

    for i in range(1):
        res2 = 0
        break
    else:
        res2 = 1

    # Then
    assert res1 == 1
    assert res2 == 0

def test_while_else():
    # Given

    # When
    i = 0
    while i < 10:
        res1 = 0
        i += 1
    else:
        res1 = 1

    i = 0
    while i < 10:
        res2 = 0
        i += 1
        break
    else:
        res2 = 1

    # Then
    assert res1 == 1
    assert res2 == 0

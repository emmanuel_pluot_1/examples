from functools import partial

def test_partial_add():
    # Given
    def add(x, y):
        return x + y

    # When
    add_five = partial(add, 5)
    res = add_five(8)

    # Then
    assert res == 8 + 5

def test_partial_create_id():
    # Given
    def create_person(first_name, last_name):
        return f'{first_name.capitalize()} {last_name.upper()}'

    # When
    create_pluot = partial(create_person, last_name='pluot')
    family = (
        create_pluot('éric'),
        create_pluot('nathalie'),
        create_pluot('florine'),
        create_pluot('aurélien'),
        create_pluot('ophélie'),
        create_pluot('emmanuel')
    )

    # Then
    assert family == (
        'Éric PLUOT',
        'Nathalie PLUOT',
        'Florine PLUOT',
        'Aurélien PLUOT',
        'Ophélie PLUOT',
        'Emmanuel PLUOT'
    )

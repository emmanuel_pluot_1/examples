---
marp: true
theme: gaia
class:
  - lead
---

# Retour sur Python

Difficile de savoir ce que l'on ignore

---

## Context : Fin sur Ansible

 * Pousser Ansible => Python
 * Poussons Python ?
 * Public pas forcément orienté dev
 * Alléger et fun...
 * Manu was here

---

## Manu scale of Python

 * Utilisée en entretiens
 * Basée sur mes connaissances
 * Change avec le temps
 * 100% subjective
 * Une intro
 * (Une occasion de mettre des noms débiles ^^)

---

## Manu scale of Python

 * Level 1: Nooooooooooooob
 * Level 2: Rookie
 * Level 3: Average Wikipedia Reader
 * Level 4: GéDéjaKodéAvant
 * Level 5: Padawan
 * Level 6: Citizen of the Pythonverse
 * Level 7: Certified Manu-proof
 * Level 8: ChatGPT Tier
 * Level 9: A new contender has emerged
 * Level 10: I know Kung-Fu


---

## Level 1: Nooooooooooooob

- **Version**
  - Dernière version de Python ?
- **Type de langage**
  - Interprété?, faiblement typé, GPL, haut niveau
- **Cas d'usage**
  - Web development, data analysis, artificial intelligence, scripting

---

## Level 2: Rookie

- **Loop**
  - for, while, range, in
- **Switch cases**
  - Pas de switch, mapping, (new) match
- **Function**
  - def, paramètres, retour, (None def ?)

---

## Level 3: Average Wikipedia Reader

- **tuple vs list vs dict vs set**
  - Tuple vs List, Dict, Set
  - Doublon dans liste ?
  - in, not
  - Notation du tuple ?
- **== vs is**
- **import**
  - import, from, as
  - import order?

---

## Level 4: GéDéjaKodéAvant

- **Exceptions**
  - try, except, finally, as
- **pass + break**
  - break in nested loops?
  - continue
- **OOP 1**
  - class
  - Constructors (+trap)
  - Property

---

## Level 5: Padawan

- **Common tools lvl1**
  - list comprehension
  - all, any, zip, sorted
  - lambda
- **typing**
- **OOP 2**
  - inheritance, multiple inheritance
  - operator overloading

---

## Level 6: Citizen of the Pythonverse

- **Maturity**
  - Lecture de log
  - Advantages/disadvantages/comparison
- **Scoping**
  - LEGB rule (Local, Enclosing, Global, Built-in)
- **OOP 3**
  - dataclass
  - staticmethod
  - classmethod

---

## Level 7: Certified Manu-proof

- **Common tools lvl2**
  - decorators
  - contexts
  - generators
  - match statement
  - deepcopy

---

## Level 7: Certified Manu-proof

- **Advanced paramters**
  - Slices
  - Variadic (args, kwargs)
  - Tout est objet
- **OOP 4**
  - Metaclass
  - getattr/setattr/delattr
  - Slots

---

## Level 8: ChatGPT Tier

- **Project setup**
  - project.toml
  - setup.cfg, setup.py
  - requirements.txt, requirements_test.txt
  - pip install -e .
  - src, tests

---

## Level 8: ChatGPT Tier

- **Tooling**
  - pytest
  - flake8 (+PEP8)
  - bandit
  - pylint
  - best practices, ignoring, correct handling

---

## Level 8: ChatGPT Tier

- **Connaissance des libs**
  - **Data**: panda, numpy, matplotlib, plotly
  - **Web**: fastapi, django, flask, requests, sqlalchemy
  - **AI**: pytorch, tensorflow, pybrain, keras
  - **UI**: pygame, tkinter, opencv, PyQt5, Kivy

---

## Level 9: A new contender has emerged

- **Common tools lvl3**
  - Coroutines
  - Descripteurs
  - Functools
    - lru_cache
    - partial
    - reduce
  - Multiprocessing

---

## Level 9: A new contender has emerged

- **Fourtout imports relou**
  - Résolution d'import circulaire
  - Mocking:
    - MagicMock
    - Monkeypatch
    - Fixtures
  - Import par nom

---

## Level 9: A new contender has emerged


- **Execution**
  - parsing des arguments
  - console_scripts
  - py2exe
  - pyinstaller
  - cx_freeze

---

## Level 10: I know Kung-Fu

- **Corner cases**
  - Int <256
  - Lambda in loops
  - For else
- **Vocab**
  - GIL (Global Interpreter Lock)
  - MRO (Method Resolution Order)
  - Ducktyping
- **Interface avec code C/C++**
  - ctypes

---

## Aujourd'hui

 * Hors de question de voir tout ca, évidemment
 * Ou placer le curseur ?
 * Histoire de varier les plaisirs : 1 de chaque
 * Mix fun facts + vrai infos/tips
 * Pas sûr d'aller au bout
 * Suite concevable

---

## Mes choix

- Type de langage
- Mapping
- Notation du tuple
- Break in nested loops
- List comprehension

---

## Mes choix

- Lecture de log
- Decorators/Contexts
- Project setup (quick overview)
- Partial
- Corner cases


---

## Type de langage

 * Interprété
 * Compilation en bytecode (cached -> pyc)
 * Plutôt semi compilé
 * Selon l'implémentation -> JIT
   * e.g. PyPy


---

## Mapping

```python
if(a == VAL_A):
    return 'a'
if(a == VAL_B):
    return 'b'
if(a == VAL_C):
    return 'c'
if(a == VAL_D):
    return 'd'
if(a == VAL_E):
    return 'e'
return 'default'
```

---

## Mapping

```python
VAL_MAPPER = {
    VAL_A: 'a',
    VAL_B: 'b',
    VAL_C: 'c',
    VAL_D: 'd',
    VAL_E: 'e'
}

...

return VAL_MAPPER.get(a, 'default')
```
---

## Notation du tuple

???

---

## Notation du tuple

Code:
```python
a = ()
print(type(a), a)
a = (0)
print(type(a), a)
a = (0, 1)
print(type(a), a)
```

Output:
```
<class 'tuple'> ()
<class 'int'> 0
<class 'tuple'> (0, 1)
```

---

## Notation du tuple

Code:
```python
a = ()
print(type(a), a)
a = 0,
print(type(a), a)
a = 0, 1
print(type(a), a)
```

Output:
```
<class 'tuple'> ()
<class 'tuple'> (0,)
<class 'tuple'> (0, 1)
```

---

## Notation du tuple

Code:
```python
a = ()
print(type(a), a)
a = (0,)
print(type(a), a)
a = (0, 1)
print(type(a), a)
```

Output:
```
<class 'tuple'> ()
<class 'tuple'> (0,)
<class 'tuple'> (0, 1)
```

---

## Break in nested loops

Code:

```python
for i in range(1, 4):  # Outer loop
    for j in range(1, 4):  # Inner loop
        if j == 2:
            break  # Exits the inner loop
print(i, j)
```

Output:
```
3 2
```


---

## List comprehension

* Construction d'une collection
* Cheminement habituel:
  * Instancier la collection
  * Peupler la collection
* Cheminement d'une liste compréhension:
  * Décrire la méthode de construction de la collection

---

## List comprehension

Sans list comprehension:
```python
a = []
for val in MY_VALUES:
  if val % 2 == 0:
    a.append(val + 2)
```

Avec list comprehension:
```python
a = [
    val + 2
    for val in MY_VALUES
    if val % 2 == 0
]
```

---

## List comprehension

Dictionnaire:
```python
a = {
    str(val): val
    for val in MY_VALUES
}
```

Ensemble:
```python
a = {
    val + 2
    for val in MY_VALUES
}
```

---

## List comprehension

Tuple:
```python
a = (
    val + 2
    for val in MY_VALUES
) # generator
```

Tuple:
```python
a = tuple(
    val + 2
    for val in MY_VALUES
)
```

---

## Lecture de log

![](imgs/log1.webp)

---

## Lecture de log

![](imgs/log2.webp)

---

## Lecture de log

![](imgs/log3.webp)

---

## Decorators/Contexts

Examples/Demo

---

## Project setup

<center>

<img src="imgs/layout1.png" style="width: 550px; height: 600px;">

</center>

---

## Project setup

<center>

<img src="imgs/layout2.png" style="width: 550px; height: 600px;">

</center>

---

# Partial

Examples/Demo

---

# Corner cases

Examples/Demo

---

# Corner cases

```c
for(int i=0; i<10; ++i){
    // code
}
```

```c
int i = 0;

while(i<10){
    // code
    ++i;
}
```

---

# Corner cases

```c
int i = 0;

before_if:
if(i<10){
    // code
    ++i;
    goto before_if
}
```

---

# Corner cases

```c
int i = 0;

before_if:
if(i<10){
    // code
    // break == goto after_if
    ++i;
    goto before_if
}
after_if:
```

---

# Corner cases

```c
int i = 0;

before_if:
if(i<10){
    // code
    // break == goto after_if
    ++i;
    goto before_if
}else{ // <<<<<---- This "else"
    // more code
}
after_if:
```

---

# Finito

Merci pour votre écoute !

Comme d'habitude, questions et insultes bienvenues :)

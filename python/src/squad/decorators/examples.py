def call_twice(my_func):
    def new_func():
        res1 = my_func()
        res2 = my_func()
        return res1, res2
    return new_func

def call_twice_mult(mult):
    def wrapper(my_func):
        def new_func():
            res1 = my_func()
            res2 = my_func()
            return res1 * mult, res2 * mult
        return new_func
    return wrapper

def add_hello(my_class):
    the_secret = 'wow'
    def hello(self):
        return the_secret
    my_class.hello = hello
    return my_class

def noned(_):
    pass
from contextlib import contextmanager

@contextmanager
def example_func(a_class):
    def lol(self):
        return 'wow'

    a_class.lol = lol

    yield 2

    delattr(a_class, 'lol')

class ExampleCls:
    def __init__(self, a_class):
        self.a_class = a_class

    def __enter__(self):
        def lol(self):
            return 'wow'
        self.a_class.lol = lol
        return 2

    def __exit__(self, type_, value, traceback):
        delattr(self.a_class, 'lol')

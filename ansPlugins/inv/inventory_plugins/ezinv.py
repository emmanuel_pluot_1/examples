import yaml

from ansible.plugins.inventory import BaseInventoryPlugin

BASE_NAT = '172.18.4'
HOST = 'example.com'

class InventoryModule(BaseInventoryPlugin):
    NAME = 'ezinv'
    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path, cache)

        with open(path, encoding='utf-8') as inv_file:
            data = yaml.safe_load(inv_file)

        del data['plugin']
        self.inventory.set_variable('all', 'ansible_connection', 'local')

        for type_, elements in data.items():
            # Type groups
            self.inventory.add_group(type_)
            self.inventory.set_variable(type_, 'type', type_)
            for element, hosts in elements.items():
                # IP groups
                self.inventory.add_group(element)
                self.inventory.add_child(type_, element)
                self.inventory.set_variable(element, 'group', element)
                for idx, host in enumerate(hosts.split()):
                   # Hosts
                   host_name = f'{element.replace(" ", "_")}{idx}.{HOST}'
                   self.inventory.add_host(host_name, group=element)
                   self.inventory.set_variable(host_name, 'ansible_host', f'{BASE_NAT}.{host}')


    def verify_file(self, path):
        with open(path, encoding='utf-8') as inv_file:
            yaml.safe_load(inv_file)
        return True
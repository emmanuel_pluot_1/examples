class TestModule:
    def tests(self):
        return {
            'even_char': lambda s: len(s) % 2 == 0,
            'odd_char': lambda s: len(s) % 2 != 0
        }

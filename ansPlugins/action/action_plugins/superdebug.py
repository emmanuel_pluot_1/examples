from ansible.plugins.action import ActionBase

class ActionModule(ActionBase):

    TRANSFERS_FILES = False

    def run(self, tmp=None, task_vars=None):
        if task_vars is None:
            task_vars = {}

        result = super(ActionModule, self).run(tmp, task_vars)

        msg = self._task.args['msg']
        nb = self._task.args.get('nb', 3)

        copy_action_task = self._task.copy()
        copy_action_task.args = {'content': msg}

        copy_action = self._shared_loader_obj.action_loader.get(
            'copy',
            task=copy_action_task,
            connection=self._connection,
            play_context=self._play_context,
            loader=self._loader,
            templar=self._templar,
            shared_loader_obj=self._shared_loader_obj
        )

        for i in range(nb):
            copy_action_task.args['dest'] = f'/tmp/debug{i}'
            failed = copy_action.run(task_vars=task_vars).get('failed', False)
            result['failed'] = result.get('failed', False) | failed

        return self._execute_module(
            module_name='file',
            module_args={
                'path': '/tmp/debug0',
                'state': 'file'
            },
            task_vars=task_vars
        )

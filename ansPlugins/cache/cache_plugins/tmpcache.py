import os
import json
import copy

from ansible.plugins.cache import BaseCacheModule


class CacheModule(BaseCacheModule):
    def __init__(self, *args, **kwargs):
        super(CacheModule, self).__init__(*args, **kwargs)
        self.cache_file = '/tmp/ansible_cache.json'
        if not os.path.exists(self.cache_file):
            self.data = {}
        else:
            with open(self.cache_file, 'r', encoding='utf-8') as f:
                self.data = json.load(f)

    def save(self):
        with open(self.cache_file, 'w', encoding='utf-8') as f:
            json.dump(self.data, f)

    def get(self, key):
        return self.data.get(key)

    def set(self, key, value):
        self.data[key] = value
        self.save()

    def delete(self, key):
        if key in self.data:
            del self.data[key]
            self.save()

    def keys(self):
        return list(self.data.keys())

    def contains(self, key):
        return key in self.data

    def flush(self):
        self.data = {}
        self.save()

    def copy(self):
        return copy.deepcopy(self.data)

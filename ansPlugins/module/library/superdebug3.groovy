#!/usr/bin/groovy

import groovy.json.JsonOutput

def args = new File(args[0]).text.split(' ').findAll{ it.contains('=') }.collectEntries {
    def (key, value) = it.split('=')
    [(key.trim()): value.trim()]
}

def msg = args['msg']
def nb = args.get('nb', '3') as int

def result = [changed: false, message: msg]

(0..<nb).each{ i ->
    def dest = new File("/tmp/ddddebug_$i")
    if(dest.exists() && dest.text == msg){ return }
    dest.text = msg
    result.changed = true
}

println JsonOutput.toJson(result)

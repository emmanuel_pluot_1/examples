#!/usr/bin/python

import os

from ansible.module_utils.basic import AnsibleModule


def main():
    module_args = dict(
        msg=dict(type='str', required=True),
        nb=dict(type='int', required=False, default=3)
    )

    result = dict(
        changed=False,
        original_message='',
        message=''
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    msg = module.params['msg']
    nb = module.params['nb']

    if module.check_mode:
        module.exit_json(**result)

    result['message'] = msg

    for i in range(nb):
        dest = f'/tmp/ddebug{i}'
        if os.path.exists(dest):
            with open(dest, 'r') as f:
                if f.read() == msg:
                    continue
        with open(dest, 'w') as f:
            f.write(msg)
            result['changed'] = True

    module.exit_json(**result)

if __name__ == '__main__':
    main()

#!/usr/bin/python

import os
import sys
import json

with open(sys.argv[1], encoding='utf-8') as f:
    content = f.read()
args = dict(eq.split('=') for eq in content.split(' ') if '=' in eq)

msg = args['msg']
nb = int(args.get('nb', 3))

result = {'changed': False, 'message': msg}

for i in range(nb):
    dest = f'/tmp/dddebug_{i}'
    if os.path.exists(dest):
        with open(dest, 'r') as f:
            if f.read() == msg:
                continue
    with open(dest, 'w') as f:
        f.write(msg)
        result['changed'] = True

print(json.dumps(result))

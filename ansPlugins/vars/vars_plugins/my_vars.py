import csv
import requests
from ansible.plugins.vars import BaseVarsPlugin

data = {}

def unified_name(name):
    return name.lower().replace(' ', '_').replace('-', '_')

class VarsModule(BaseVarsPlugin):
    def get_vars(self, loader, path, entities, cache=True):
        global data
        super(VarsModule, self).get_vars(loader, path, entities)

        spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1RZRTK0jDFkBZxmsP1Zw8JoNEyxtTWW7Icy8x9xA6wWc/gviz/tq?tqx=out:csv&gid=1986235519'


        if not data:
            response = requests.get(spreadsheet_url)
            reader = csv.reader(response.text.splitlines())

            headers = next(reader)

            for row in reader:
                for i, cell in enumerate(row):
                    if cell:
                        data.setdefault(unified_name(headers[i]), []).append(cell)

        return data

from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
import requests

class LookupModule(LookupBase):
    def run(self, term, variables=None, **kwargs):
        return [requests.get(f'http://localhost:5000/key/{term[0]}').json()['dey']]

from flask import Flask
import yaml

app = Flask(__name__)

with open('values.yaml', 'r', encoding='utf-8') as file:
    config_data = yaml.safe_load(file)

@app.route('/key/<name>')
def get_key_value(name):
    return {'dey': config_data.get(name)}

if __name__ == '__main__':
    app.run()
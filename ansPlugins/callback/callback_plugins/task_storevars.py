import json

from ansible.plugins.callback import CallbackBase

class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'task_var_store_callback'

    def __init__(self):
        super(CallbackModule, self).__init__()
        self.task_vars = {}

    def v2_runner_on_ok(self, result, **kwargs):
        self.task_vars[str(result._task)] = result._task.vars

    def v2_playbook_on_stats(self, _):
        print('Collected task results:')
        print(json.dumps(self.task_vars, indent=4))

import json

from ansible.plugins.callback import CallbackBase

class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 2.0
    CALLBACK_NAME = 'task_results_callback'

    def __init__(self):
        super(CallbackModule, self).__init__()
        self.task_results = {}

    def v2_runner_on_ok(self, result, **kwargs):
        task_name = str(result._task)
        store_result = result._task.vars.get('store_result', False)
        result_key = result._task.vars.get('result_key', task_name)

        if store_result:
            host = result._host.get_name()
            if result_key not in self.task_results:
                self.task_results[result_key] = {}
            self.task_results[result_key][host] = result._result.get('stdout')

    def v2_playbook_on_stats(self, stats):
        print('Collected task results:')
        print(json.dumps(self.task_results, indent=4))
